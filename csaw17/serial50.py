#!/usr/bin/python

import sys
import socket
import binascii

#csaw17 serial

host = 'misc.chal.csaw.io'
port = 4239

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((host, port))

def main():
    while 1:
        output = ''
        _i = int(0)
        while 1:
            response = ''
            data = client.recv(4096)
            response += data
            if len(data) < 1:
                break
            lastline = response.splitlines()[-1]
            print(response)
            ll_sum = sum( [int(i) for i in lastline[1:9]])
            parity = lastline[9]
#            print('bits count =%d, parity biti = %s' % (ll_sum, parity)  )
            if int(parity) == (0 if ll_sum%2 == 0 else 1):  #raw_input()
               buf = '1'
               n = int('0b'+ lastline[1:9], 2)
               output += binascii.unhexlify('%x' % n)
            else:
                buf = '0'
            print('answer %s' % buf)
            buf += '/n'

            client.send(buf)
            print('%d - %s' %(_i, output))
            _i += 1
if __name__ == '__main__':
    try:
        main()
    except:
        sys.exit(1)

#answer 0
#999 - flag{@n_int3rface_betw33n_data_term1nal_3quipment_and_d@t@_circuit-term1nating_3quipment}
#00000101001

